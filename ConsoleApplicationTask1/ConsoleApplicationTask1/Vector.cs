﻿using System;
using System.Collections;

namespace ConsoleApplicationTask1
{
    /// <summary>
    /// Represents array of objects of type int32 that can be accessed by index. Provides methods for adding, subtracting, multiplying array
    /// </summary>
    public class Vector:IEnumerable
    {
        private int _count;
        private int _startIndex;
        private int _endIndex;
        private readonly int[] _array;

        /// <summary>
        /// Initializes a new instance Vector class that is empty and has the specified size.
        /// </summary>
        /// <param name="size">The number of elements that the new array can store.</param>
        public Vector(int size)
        {
            _count = size;
            _array = new int[_count];
            _startIndex = 0;
            _endIndex = size - 1;
        }
        /// <summary>
        /// Initializes a new instance of the Vector class that is empty and has the specified bounds.е
        /// </summary>
        /// <param name="startIndex">The starting index</param>
        /// <param name="endIndex">The final index</param>
        public Vector(int startIndex, int endIndex)
        {
            if (startIndex >= endIndex)
            {
                throw new VectorIndexOutOfRangeException(
                    "The range is incorrect. The initial index must be less than the final index");
            }
            _startIndex = startIndex;
            _endIndex = endIndex;
            _count = endIndex - startIndex + 1;
            _array = new int[_count];
        }
        /// <summary>
        /// Gets the number of elements contained in the array
        /// </summary>
        public int Count
        {
            get { return _count; }
        }
        /// <summary>
        /// Gets the first index of elements contained in the array
        /// </summary>
        public int StartIndex
        {
            get { return _startIndex; }
        }
        /// <summary>
        /// Gets the last index of elements contained in the array
        /// </summary>
        public int EndIndex
        {
            get { return _endIndex; }
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index, counted from the initial element, to get or set.</param>
        /// <returns>Returns a value at the specified index</returns>
        public int this[int index]
        {
            get
            {
                int correctIndex=CheckIndex(index);
                return _array[correctIndex];
            }
            set
            {
                int correctIndex = CheckIndex(index);
                _array[correctIndex] = value;
            }
        }

        /// <summary>
        /// Summarizes the values ​​of two arrays
        /// </summary>
        /// <param name="vector1">The first array of elements which will be added</param>
        /// <param name="vector2">The second array of elements which will be added</param>
        /// <returns>A new array obtained as a result of adding elements of two incoming</returns>
        public static Vector SumArray(Vector vector1, Vector vector2)
        {
            if (vector1._startIndex != vector2._startIndex && vector1._endIndex!=vector2._endIndex)
            {
                throw new ArgumentException("The boundaries of arrays are not the same");
            }

            Vector temp = new Vector(vector1.Count);
            for (int i = vector1._startIndex; i <= vector2._endIndex; i++)
            {
                temp[i] = vector1[i] + vector2[i];
            }
            return temp;
        }
        /// <summary>
        /// Summarizes the values ​​of two arrays
        /// </summary>
        /// <param name="vector1">The first array of elements which will be added</param>
        /// <param name="vector2">The second array of elements which will be added</param>
        /// <returns>A new array obtained as a result of adding elements of two incoming</returns>
        public static Vector operator +(Vector vector1, Vector vector2)
        {
            return SumArray(vector1, vector2);
        }

        /// <summary>
        /// Subtracts the values of two arrays
        /// </summary>
        /// <param name="vector1">The first array of elements from which will be subtracted</param>
        /// <param name="vector2">The second array of elements to be deducted</param>
        /// <returns>A new array obtained as a result of subtraction of elements from two incoming</returns>
        public static Vector SubtractArray(Vector vector1, Vector vector2)
        {
            if (vector1._startIndex != vector2._startIndex && vector1._endIndex != vector2._endIndex)
            {
                throw new ArgumentException("The boundaries of arrays are not the same");
            }

            Vector temp = new Vector(vector1.Count);
            for (int i = vector1._startIndex; i <= vector1._endIndex; i++)
            {
                temp[i] = vector1[i] - vector2[i];
            }
            return temp;
        }
        /// <summary>
        /// Subtracts the values of two arrays
        /// </summary>
        /// <param name="vector1">The first array of elements from which will be subtracted</param>
        /// <param name="vector2">The second array of elements to be deducted</param>
        /// <returns>A new array obtained as a result of subtraction of elements from two incoming</returns>
        public static Vector operator -(Vector vector1, Vector vector2)
        {
            return SubtractArray(vector1, vector2);
        }

        /// <summary>
        /// Multiplies the elements of array by the specified value
        /// </summary>
        /// <param name="vector">The array whose elements will be multiplied</param>
        /// <param name="value">Value</param>
        /// <returns>A new array obtained as a result of the multiplication elements</returns>
        public static Vector Multiply(Vector vector, int value)
        {
            Vector temp = new Vector(vector.Count);
            for (int i = vector._startIndex; i < vector._endIndex; i++)
            {
                temp[i] = vector[i] * value;
            }
            return temp;
        }
        /// <summary>
        /// Compares arrays by value
        /// </summary>
        /// <param name="obj">You can pass the array as an object</param>
        /// <returns>The comparison value</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            Vector temp = obj as Vector;
            if (temp == null)
            {
                return false;
            }
            if (temp._startIndex != _startIndex || temp._endIndex != _endIndex)
            {
                return false;
            }
            for (int i = temp._startIndex; i < temp._endIndex; i++)
            {
                if (this[i] != temp[i])
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// Compares arrays by value
        /// </summary>
        /// <param name="vector">Value</param>
        /// <returns>The comparison value</returns>
        public bool Equals(Vector vector)
        {
            if (vector == null)
            {
                return false;
            }
            if (vector._startIndex != _startIndex || vector._endIndex != _endIndex)
            {
                return false;
            }
            for (int i = vector._startIndex; i < vector.Count; i++)
            {
                if (this[i] != vector[i])
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode()
        {
            if (_array != null)
            {
                unchecked
                {
                    int hash = 27;
                    
                    foreach (var item in _array)
                    {
                        hash = hash * 13 + item.GetHashCode();
                    }
                    return hash;
                }
            }
            return 0;
        }
        
        private int CheckIndex(int index)
        {
            if (index < _startIndex || index > _endIndex)
            {
                string message = string.Format("The index {0} is outside the range of {1} to {2}", index,
                    _startIndex, _endIndex);
                throw new VectorIndexOutOfRangeException(message);
            }
            return index - _startIndex;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return _array.GetEnumerator();
        }
    }


    [Serializable]
    public class VectorIndexOutOfRangeException : ApplicationException
    {
        public VectorIndexOutOfRangeException()
        {
        }

        public VectorIndexOutOfRangeException(string message) : base(message)
        {
        }

        public VectorIndexOutOfRangeException(string message, Exception ex) : base(message)
        {
        }

        protected VectorIndexOutOfRangeException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext contex)
            : base(info, contex)
        {
        }
        //Git test
    }
}