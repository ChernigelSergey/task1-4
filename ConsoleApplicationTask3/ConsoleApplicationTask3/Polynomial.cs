﻿using System;
using System.Linq;
using System.Text;


namespace ConsoleApplicationTask3
{
    public class Polynomial
    {
        private double[] _coefficients;

        ///<summary>
        /// Create a polynomial based on the coefficients.
        ///</summary>
        ///<param name = "coefficients">The coefficients of the polynomial.</param>
        public Polynomial(params double[] coefficients)
        {
            _coefficients = coefficients;
        }

        /// <summary>
        /// Gets or sets the value of the coefficient of the polynomial.
        /// </summary>
        ///<param name = "n">Index of coefficient.</param>
        ///<returns>Value of coefficient.</returns>
        public double this[int n]
        {
            get { return _coefficients[n]; }
            set { _coefficients[n] = value; }
        }
        /// <summary>
        /// Convert polynomial in to a human-readable string.
        /// </summary>
        public override string ToString()
        {
            StringBuilder polynomial = new StringBuilder();
            for (int i = 0; i < _coefficients.Length - 1; i++)
            {
                polynomial.Append(_coefficients[i] + "x^" + (_coefficients.Length - (i + 1)));
                if ((i + 1) < _coefficients.Length)
                {
                    polynomial.Append("+");
                }
            }
            polynomial.Append(_coefficients.Last());
            return polynomial.ToString();
        }
        /// <summary>
        /// Calculate polynomail with parametr
        /// </summary>
        /// <param name="x">Parametr of polynomial</param>
        /// <returns>Value</returns>
        public double Calculate(double x)
        {
            double result = 0;
            for (int i = 0; i < _coefficients.Length; i++)
            {
                result += _coefficients[i]*Math.Pow(x, ((_coefficients.Length - 1) - i));
            }
            return result;
        }
        private static Polynomial GetSum(Polynomial max, Polynomial min)
        {
            var result = new double[max._coefficients.Length];
            int difference = max._coefficients.Length - min._coefficients.Length;

            for (int i = 0; i < difference; i++)
            {
                result[i] = max[i];
            }
            for (int i = 0; i < min._coefficients.Length; i++)
            {
                result[i + difference] = max[i + difference] + min[i];
            }
            return new Polynomial(result);
        }
        /// <summary>
        /// Adds two polynomial
        /// </summary>
        /// <param name="first">First polynomial</param>
        /// <param name="second">Second polynomial</param>
        /// <returns>The resulting polynomial</returns>
        public static Polynomial Summation(Polynomial first, Polynomial second)
        {
            if (first._coefficients.Length >= second._coefficients.Length)
            {
                return GetSum(first, second);
            }
            else
            {
                return GetSum(second, first);
            }
        }
        /// <summary>
        /// Subtract two polynomial
        /// </summary>
        /// <param name="first">First polynomial</param>
        /// <param name="second">Second polynomial</param>
        /// <returns>The resulting polynomial</returns>
        public static Polynomial Subtraction(Polynomial first, Polynomial second)
        {
            if (first._coefficients.Length >= second._coefficients.Length)
            {
                return GetSubstract(first, second);
            }
            else
            {
                return GetSubstract(second, first);
            }
        }
        private static Polynomial GetSubstract(Polynomial max, Polynomial min)
        {
            var result = new double[max._coefficients.Length];
            int difference = max._coefficients.Length - min._coefficients.Length;

            for (int i = 0; i < difference; i++)
            {
                result[i] = max[i];
            }
            for (int i = 0; i < min._coefficients.Length; i++)
            {
                result[i + difference] = max[i + difference] - min[i];
            }
            return new Polynomial(result);
        }

        /// <summary>
        /// Multiplies polynomials
        /// </summary>
        /// <param name="first">First polynomial</param>
        /// <param name="second">Second polynomial</param>
        /// <returns>The resulting polynomial</returns>
        public static Polynomial Multiplication(Polynomial first, Polynomial second)
        {
            int itemsCount = first._coefficients.Length + second._coefficients.Length - 1;
            var result = new double[itemsCount];
            for (int i = 0; i < first._coefficients.Length; i++)
            {
                for (int j = 0; j < second._coefficients.Length; j++)
                {
                    result[i + j] += first[i]*second[j];
                }
            }
            return new Polynomial(result);
        }
    }
}